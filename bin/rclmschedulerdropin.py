#!/usr/bin/python

import sys

global oppCount
global teamCount
global leftTeam
global rightTeam
global TOTAL_TEAMS

MAX_TEAM_SIZE = 10
AGENTS_PER_TEAM = 2

def allOpponentsPlayed():
  for i in range(len(oppCount)):
    for j in range(i+1,len(oppCount[i])):
      if oppCount[i][j] == 0:
        return False
  return True

def updateCounts():
  for i in range(len(leftTeam)):
    for j in range(len(leftTeam)):
      teamCount[leftTeam[i]][leftTeam[j]] = teamCount[leftTeam[i]][leftTeam[j]]+1
    for j in range(len(rightTeam)):
      oppCount[leftTeam[i]][rightTeam[j]] = oppCount[leftTeam[i]][rightTeam[j]]+1
  for i in range(len(rightTeam)):
    for j in range(len(rightTeam)):
      teamCount[rightTeam[i]][rightTeam[j]] = teamCount[rightTeam[i]][rightTeam[j]]+1
    for j in range(len(leftTeam)):
      oppCount[rightTeam[i]][leftTeam[j]] = oppCount[rightTeam[i]][leftTeam[j]]+1

def getBetterAgent(x,y,position,tieBreaker):
  xGamesPlayed = teamCount[x][x]
  yGamesPlayed = teamCount[y][y]

  # Give preference to those who have played fewer games
  
  if xGamesPlayed > yGamesPlayed:
    return y
  elif yGamesPlayed > xGamesPlayed:
    return x
  

  myTeam = leftTeam
  opponentTeam = rightTeam
  if position%2 == 1:
    myTeam = rightTeam
    opponentTeam = leftTeam
   
  xOpponentNew = 0
  xOpponentScore = 0
  yOpponentNew = 0
  yOpponentScore = 0
  for i in range(len(opponentTeam)):
    if oppCount[x][opponentTeam[i]] == 0:
      xOpponentNew = xOpponentNew+1
    else:
      xOpponentScore = xOpponentScore+oppCount[x][opponentTeam[i]]

    if oppCount[y][opponentTeam[i]] == 0:
      yOpponentNew = yOpponentNew+1
    else:
      yOpponentScore = yOpponentScore+oppCount[y][opponentTeam[i]]

  # Give preference to creating more new opponents
  
  if xOpponentNew > yOpponentNew:
    return x
  elif yOpponentNew > xOpponentNew:
    return y

  xTeamNew = 0
  xTeamScore = 0
  yTeamNew = 0
  yTeamScore = 0
  for i in range(len(myTeam)):
    if teamCount[x][myTeam[i]] == 0:
      xTeamNew = xTeamNew+1
    else:
      xTeamScore = xTeamScore+teamCount[x][myTeam[i]]^2

    if teamCount[y][myTeam[i]] == 0:
      yTeamNew = yTeamNew+1
    else:
      yTeamScore = yTeamScore+teamCount[y][myTeam[i]]^2

  # Give preference for creating new teammates
  
  if xTeamNew > yTeamNew:
    return x
  elif yTeamNew > xTeamNew:
    return y

  xScore = xTeamScore+xOpponentScore
  yScore = yTeamScore+yOpponentScore

  # Preference to whoever has the lowest measured opponent/teammate score
  if xScore > yScore:
    return y
  if yScore > xScore:
    return x

  # Use tie breaker for otherwise equal choices
  xTieBreak = x-tieBreaker
  if xTieBreak < 0:
    xTieBreak = xTieBreak+TOTAL_TEAMS
  yTieBreak = y-tieBreaker
  if yTieBreak < 0:
    yTieBreak = yTieBreak+TOTAL_TEAMS
 
  if yTieBreak < xTieBreak:
    return y
  
  return x


  

if len(sys.argv) < 3:
  print "usage: " + sys.argv[0] + " <teams_input_file> <schedule_ouput_file> [min_games]"
  sys.exit()

minGames = 0
if len(sys.argv) > 3:
  minGames = int(sys.argv[3])

teams = []
teamSymbols = {}
f = open(sys.argv[1])
lines = f.readlines()
for l in lines:
  tokens = l.split()
  team = tokens[0]
  symbol = tokens[1]
  teams.append(team)
  teamSymbols[team] = symbol
f.close()


TOTAL_TEAMS = len(teams)

teamSize = int(round(TOTAL_TEAMS/2.0))

if AGENTS_PER_TEAM > MAX_TEAM_SIZE:
  print "Error: agents_per_team must be less than or equal to " + str(MAX_TEAM_SIZE)
  sys.exit()

teamSize = min(teamSize, MAX_TEAM_SIZE/AGENTS_PER_TEAM)


teamCount = [[0 for x in xrange(TOTAL_TEAMS)] for x in xrange(TOTAL_TEAMS)]
oppCount = [[0 for x in xrange(TOTAL_TEAMS)] for x in xrange(TOTAL_TEAMS)] 


tieBreaker = 0
games = 0
gameArrays = []
while not allOpponentsPlayed() or games < minGames:
  agents = range(TOTAL_TEAMS)
  leftTeam = []
  rightTeam = []
  for i in xrange(min(teamSize*2, TOTAL_TEAMS)):
    bestAgent = agents[0]
    for j in xrange(1,len(agents)):
      bestAgent = getBetterAgent(bestAgent, agents[j], i, tieBreaker)
    if i%2 == 1:
      rightTeam.append(bestAgent)
    else:
      leftTeam.append(bestAgent)
    agents.remove(bestAgent)
    tieBreaker = (tieBreaker+1)%TOTAL_TEAMS
  updateCounts()
  gameArrays.append([leftTeam, rightTeam])
  games = games+1
  print "Game " + str(games) + ":"
  print "Left:\t" + str(leftTeam)
  print "Right:\t" + str(rightTeam)
  print
  
print "Total games = " + str(games)
print

print "Opponent/Teammate Count:"
print "\t".expandtabs(4),
for i in range(TOTAL_TEAMS):
    print "p" + str(i+1).zfill(2) + "\t".expandtabs(0),
print

for i in range(TOTAL_TEAMS):
    print "p" + str(i+1).zfill(2) + "\t".expandtabs(1),
    for j in range(TOTAL_TEAMS):
        print str(oppCount[i][j]) + "/" + str(teamCount[i][j]) + "\t".expandtabs(0),
    print

f = open(sys.argv[2], 'w')
for g in gameArrays:
  leftTeamStr = ""
  leftTeamName = ""
  for p in g[0]:
    for i in range(AGENTS_PER_TEAM):
      leftTeamStr += teams[p] + " "
    leftTeamName += teamSymbols[teams[p]]
  rightTeamStr = ""
  rightTeamName = ""
  for p in g[1]:
    for i in range(AGENTS_PER_TEAM):
      rightTeamStr += teams[p] + " "
    rightTeamName += teamSymbols[teams[p]]
  f.write(leftTeamName + " " + leftTeamStr + "vs " + rightTeamName + " " + rightTeamStr + "\n")

f.close() 





